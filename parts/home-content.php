 <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> 
            itemscope itemtype="https://schema.org/Article">
        <div class="melody-home-post-wrap"> 

            <div class="excerpt-post-heading">
                <a class="excerptwrap-link"
                    href ="<?php echo esc_url( get_attachment_link( get_post_thumbnail_id() ) ); ?>" 
                    title="<?php the_title_attribute( 'before=Permalink to: &after=' ); ?>">
                
                <?php 
                if ( has_post_thumbnail() ) { 
                    $thuri = wp_get_attachment_url( get_post_thumbnail_id() ); 
                ?>
                
                <div class="linked-attachment-container" 
                    title="<?php echo esc_attr(get_the_title()); ?>" 
                    style="background: url(<?php echo esc_url( $thuri ); ?>)no-repeat;background-size: 100% 100%;">
                </div>

                <?php 
                } else { 
                    $melogo_id = get_theme_mod( 'custom_logo' );
                    $nouri  = wp_get_attachment_image_src( $melogo_id , 'thumbnail' );
                ?>
                <span class="linked-attachment-container no-thumb" 
                    style="background: url(<?php echo esc_url( $nouri[0] ); ?>)no-repeat;background-size: 100% 100%;">
                </span>
                <?php 
                } 
                ?>

                </a>
                </figure>

                
            </div>

            <div class="melody-excerpt-wrap">
                <header class="post-excerpt-heading">
                    <h2 class="melody-post-heading"><?php the_title( sprintf( '<span class="post-title"><a href="%s" rel="bookmark">', 
                        esc_attr( esc_url( get_permalink() ) ) 
                        ), '</a></span>' 
                    ); ?></h2>
                </header>
                    <div class="inner-article-content">

                        <?php
                        $enumb = get_theme_mod( 'melody_excerpt_length', 50 );
                        $excerpt = wp_trim_words( get_the_content(), 
                                    $enumb, '' );
                        echo $excerpt . ' ' 
                        . '<a href="'.get_the_permalink().'">More Link</a>';
                        ?>
                        <blockquote class="melody-comments">
                            <?php 
                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) {
                                printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 
                                        'comments title', 'melody' ), 
                                        number_format_i18n( get_comments_number() ) . ' # '
                                    );
                            } ?>
                        </blockquote>
                    </div> 
                        
            </div>

        </div>
    </article>
  
    <?php endwhile; ?>
    <nav class="pagination-nav">
            <p><span class="nav-previous alignleft">
            <?php previous_posts_link( '<span class="prevpst-nav"> < </span>' ); ?>
            </span>
            <span class="algn-cntr">
                <?php do_action( 'flexline_check_pagination' ); ?>
            </span>
            <span class="nav-next alignright">
                <?php next_posts_link( '<span class="nextpst-nav"> > </span>' ); ?>
            </span></p>
        </nav>
    <?php else : ?>
            
        <div class="post-content">
		        
            <?php echo esc_url( home_url('/') ); ?>
            
        </div>

    <?php endif; ?>         