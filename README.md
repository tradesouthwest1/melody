# Melody

- Contributors: Tradesouthwest
- Requires PHP: 7.2
- Requires CP:  1.4
- Tested up to: 1.5
- Version:      1.0.0
- License:      GPLv2 or later
- License URI:  http://www.gnu.org/licenses/gpl-2.0.html
- Tags: two-columns, translation-ready

## Description 

Melody is a theme that you can look up to and smile at. Demo at https://melody.tradesnet.us/

## Features 
- Two wide and three wide page layouts
- Grid and flex CSS layout ready for mobile and all devices
- Full content menus for wiki-like navigation

- Featured image captions 
- Logo upload
- Background color of full page
